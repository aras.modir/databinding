package com.modir.databinding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.modir.databinding.databinding.RecyclerViewItemBinding

internal class RecyclerViewAdapter(private val list: List<Book>) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: RecyclerViewItemBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.recycler_view_item, parent, false
            )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        holder.binding.title.text = list[position].title
        holder.binding.author.text = list[position].author
        holder.binding.cover.setImageDrawable(list[position].image)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolder(val binding: RecyclerViewItemBinding) :
        RecyclerView.ViewHolder(binding.root)

}