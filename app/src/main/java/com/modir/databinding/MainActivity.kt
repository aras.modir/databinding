package com.modir.databinding

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.modir.databinding.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var bookList: MutableList<Book>
    private lateinit var adapter: RecyclerViewAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        bookList = arrayListOf()
        bookList.add(
            Book(
                title = "The State of Grace",
                author = "Lois Lewandoski",
                image = getDrawable(R.drawable.book)!!
            )
        )
        bookList.add(
            Book(
                title = "The State of Grace",
                author = "Lois Lewandoski",
                image = getDrawable(R.drawable.book)!!
            )
        )
        bookList.add(
            Book(
                title = "The State of Grace",
                author = "Lois Lewandoski",
                image = getDrawable(R.drawable.book)!!
            )
        )
        bookList.add(
            Book(
                title = "The State of Grace",
                author = "Lois Lewandoski",
                image = getDrawable(R.drawable.book)!!
            )
        )
        bookList.add(
            Book(
                title = "The State of Grace",
                author = "Lois Lewandoski",
                image = getDrawable(R.drawable.book)!!
            )
        )
        bookList.add(
            Book(
                title = "The State of Grace",
                author = "Lois Lewandoski",
                image = getDrawable(R.drawable.book)!!
            )
        )
        bookList.add(
            Book(
                title = "The State of Grace",
                author = "Lois Lewandoski",
                image = getDrawable(R.drawable.book)!!
            )
        )
        bookList.add(
            Book(
                title = "The State of Grace",
                author = "Lois Lewandoski",
                image = getDrawable(R.drawable.book)!!
            )
        )
        bookList.add(
            Book(
                title = "The State of Grace",
                author = "Lois Lewandoski",
                image = getDrawable(R.drawable.book)!!
            )
        )
        bookList.add(
            Book(
                title = "The State of Grace",
                author = "Lois Lewandoski",
                image = getDrawable(R.drawable.book)!!
            )
        )

        adapter = RecyclerViewAdapter(bookList)
        binding.recyclerView.layoutManager = GridLayoutManager(this, 2)
        binding.recyclerView.adapter = adapter


    }
}