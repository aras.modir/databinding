package com.modir.databinding

import android.graphics.drawable.Drawable

internal class Book(val title: String, val author: String, val image: Drawable)